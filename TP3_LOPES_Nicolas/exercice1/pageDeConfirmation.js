function getUserName(){
    var userName = returnParameter("name");
    document.getElementById("userName").innerHTML = userName;
}

function returnParameter(parameter){
    var urlIs = window.location.href;
    var url = new URL(urlIs);
    var returnValue = url.searchParams.get(parameter);
    return returnValue;
}

getUserName();