var automatedAsk = 0;
changerCouleur();

function changerCouleur() {
  if (automatedAsk == 0) {
    selectedColor = prompt("Choisisez une couleur entre : gris | bleu | vert");
    automatedAsk += 1;
  } else {
    selectedColor = document.getElementById("colorForm").elements["color"]
      .value;
  }

  switch (selectedColor) {
    case "gris":
      choosenColor = "#C0C0C0";
      break;

    case "bleu":
      choosenColor = "#669999";
      break;

    case "vert":
      choosenColor = "#99FF66";
      break;

    default:
      choosenColor = "#FFFFFF";
      break;
  }

  document.body.style.backgroundColor = choosenColor;
}
